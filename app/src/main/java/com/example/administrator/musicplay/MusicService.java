package com.example.administrator.musicplay;

/**
 * Created by winner on 2018/10/15.
 */

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.example.administrator.data.MusicList;

import java.util.Random;

public class MusicService extends Service {

    // 播放控制命令
    public static final int COMMAND_UNKNOWN = -1;
    public static final int COMMAND_PLAY = 0;
    public static final int COMMAND_PAUSE = 1;
    public static final int COMMAND_STOP = 2;
    public static final int COMMAND_RESUME = 3;
    public static final int COMMAND_PREVIOUS = 4;
    public static final int COMMAND_NEXT = 5;
    public static final int COMMAND_CHECK_IS_PLAYING = 6;
    public static final int COMMAND_SEEK_TO = 7;
    public static final int COMMAND_RANDOM=8;
    // 播放器状态
    public static final int STATUS_PLAYING = 0;
    public static final int STATUS_PAUSED = 1;
    public static final int STATUS_STOPPED = 2;
    public static final int STATUS_COMPLETED = 3;
    // 广播标志
    public static final String BROADCAST_MUSICSERVICE_CONTROL = "MusicService.ACTION_CONTROL";
    public static final String BROADCAST_MUSICSERVICE_UPDATE_STATUS = "MusicService.ACTION_UPDATE";
    //歌曲序号
    private int number = 0;
    private int number_current=-1;
    //歌曲状态
    private int status;
    // 媒体播放类
    private MediaPlayer player = new MediaPlayer();
    // 广播接收器
    private CommandReceiver receiver;
    //电话状态
    private boolean phone = false;
    //通知栏,向系统发送全局通知
    private NotificationManager manager;

    @Override
    public void onCreate()
    {
        super.onCreate();
        // 绑定广播接收器
        bindCommandReceiver();
        //初始状态为停止
        status = MusicService.STATUS_STOPPED;
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(new MyPhoneListener(), PhoneStateListener.LISTEN_CALL_STATE);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent arg0)
    {
        return null;
    }

    //更新通知栏
    public void updateNotification()
    {
        manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        //获取自定义布局
        RemoteViews views = new RemoteViews(getPackageName(), R.layout.statusbar);
        //设置小图标，标题和内容
        views.setImageViewResource(R.id.icon, R.drawable.ic_allapps_pressed);
        views.setTextViewText(R.id.musicName, MusicList.getMusicList().get(number).getmusicName());
        views.setTextViewText(R.id.artist, MusicList.getMusicList().get(number).getmusicArtist());
        views.setImageViewResource(R.id.play_previous,R.drawable.button_play_previous);
        views.setImageViewResource(R.id.play_play,R.drawable.button_play_play);
        views.setImageViewResource(R.id.play_pause,R.drawable.button_play_pause);
        views.setImageViewResource(R.id.play_next,R.drawable.button_play_next);
        views.setImageViewResource(R.id.close,R.drawable.button_close);
        views.setViewVisibility(R.id.play_play,View.GONE);

        //歌曲是否正在播放
        if (status == MusicService.STATUS_PLAYING)
        {
            views.setViewVisibility(R.id.play_play, View.GONE);
            views.setViewVisibility(R.id.play_pause, View.VISIBLE);
        } else {
            views.setViewVisibility(R.id.play_play, View.VISIBLE);
            views.setViewVisibility(R.id.play_pause, View.GONE);
        }
        //设置监听器
        views.setOnClickPendingIntent(R.id.play_previous, pre_PendingIntent());
        views.setOnClickPendingIntent(R.id.play_play, play_PendingIntent());
        views.setOnClickPendingIntent(R.id.play_pause, pause_PendingIntent());
        views.setOnClickPendingIntent(R.id.play_next, next_PendingIntent());
        views.setOnClickPendingIntent(R.id.close, close_PendingIntent());

        Intent intent = new Intent(this,MainActivity.class);
        PendingIntent pi = PendingIntent.getActivity(this, 0, intent, 0);
        builder.setContent(views)
                .setContentIntent(pi)
                .setPriority(Notification.PRIORITY_MAX)
                .setOngoing(true)
                .setSmallIcon(R.drawable.ap_icon_on);
        Notification notification = builder.build();
        notification.flags = Notification.FLAG_ONGOING_EVENT;
        manager.notify(0x3131,notification);
    }

    //上一首
    private PendingIntent pre_PendingIntent()
    {
        Intent intent = new Intent("PREVIOUS");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
        return pendingIntent;
    }

    //播放
    private PendingIntent play_PendingIntent()
    {
        Intent intent = new Intent("RESUME");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
        return pendingIntent;
    }

    //暂停
    private PendingIntent pause_PendingIntent()
    {
        Intent intent = new Intent("PAUSE");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
        return pendingIntent;
    }

    //下一首
    private PendingIntent next_PendingIntent()
    {
        Intent intent = new Intent("NEXT");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
        return pendingIntent;
    }

    //关闭
    private PendingIntent close_PendingIntent()
    {
        Intent intent = new Intent("CLOSE");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
        return pendingIntent;
    }

    //绑定广播接收器
    private void bindCommandReceiver()
    {
        receiver = new CommandReceiver();
        IntentFilter filter = new IntentFilter(BROADCAST_MUSICSERVICE_CONTROL);
        filter.addAction("RESUME");
        filter.addAction("PAUSE");
        filter.addAction("PREVIOUS");
        filter.addAction("NEXT");
        filter.addAction("CLOSE");
        filter.addAction("SLEEPTIME");
        registerReceiver(receiver, filter);
    }

    //内部类，接收广播命令，并执行操作
    class CommandReceiver extends BroadcastReceiver
    {
        //接收命令
        @Override
        public void onReceive(Context context, Intent intent)
        {
            String ctrl_code = intent.getAction();

            if (BROADCAST_MUSICSERVICE_CONTROL.equals(ctrl_code))
            {
                //获取命令
                int command = intent.getIntExtra("command", COMMAND_UNKNOWN);
                //执行命令
                switch (command)
                {
                    case COMMAND_SEEK_TO:
                        seekTo(intent.getIntExtra("time", 0));
                        break;
                    case COMMAND_PLAY:
                        number = intent.getIntExtra("number", 0);
                        play(number);
                        break;
                    case COMMAND_PREVIOUS:
                        moveNumberToPrevious();
                        break;
                    case COMMAND_NEXT:
                        moveNumberToNext();
                        break;
                    case COMMAND_PAUSE:
                        pause();
                        break;
                    case COMMAND_STOP:
                        stop();
                        break;
                    case COMMAND_RESUME:
                        resume();
                        break;
                    case COMMAND_CHECK_IS_PLAYING:
                        checkIsPlaying();
                        break;
                    case COMMAND_RANDOM:
                        random();
                        break;
                    case COMMAND_UNKNOWN:
                    default:
                        break;
                }
            }else if ("RESUME".equals(ctrl_code)){
                resume();
            }else if ("PAUSE".equals(ctrl_code)){
                pause();
            }else if ("PREVIOUS".equals(ctrl_code)){
                moveNumberToPrevious();
            }else if ("NEXT".equals(ctrl_code)){
                moveNumberToNext();
            }else if ("CLOSE".equals(ctrl_code)){
                manager.cancel(0x3131);
                System.exit(0);
            }
        }
    }

    //读取音乐文件
    private void load(int number)
    {
        try {
            player.reset();
            player.setDataSource(MusicList.getMusicList().get(number).getmusicPath());
            player.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //注册监听器
        player.setOnCompletionListener(completionListener);
    }

    //进度条
    private void seekTo(int time)
    {
        player.seekTo(time);
        status = MusicService.STATUS_PLAYING;
        sendBroadcastOnStatusChanged(MusicService.STATUS_PLAYING);
        updateNotification();
    }

    //播放音乐
    private void play(int number)
    {
        //点击同一首歌
        if(number_current==number)
        {
            if (player != null && player.isPlaying())
            {
                player.pause();
                status = MusicService.STATUS_PAUSED;
                sendBroadcastOnStatusChanged(MusicService.STATUS_PAUSED);
                updateNotification();
            }else{
                player.start();
                status = MusicService.STATUS_PLAYING;
                sendBroadcastOnStatusChanged(MusicService.STATUS_PLAYING);
                updateNotification();
            }
        } else {
            if (player != null && player.isPlaying())
            {
                player.stop();
            }
            number_current=number;
            load(number);
            player.start();
            status = MusicService.STATUS_PLAYING;
            sendBroadcastOnStatusChanged(MusicService.STATUS_PLAYING);
            updateNotification();
        }
    }

    //选择上一首
    private void moveNumberToPrevious()
    {
        // 判断是否到达列表顶端
        if (number == 0)
        {
            Toast.makeText(MusicService.this,"已达到列表顶端",Toast.LENGTH_SHORT).show();
        } else {
            --number;
            play(number);
        }
    }

    //选择下一首
    private void moveNumberToNext()
    {
        // 判断是否到达列表底端
        if ((number ) == MusicList.getMusicList().size()-1)
        {
            Toast.makeText(MusicService.this,"已达到列表底端",Toast.LENGTH_SHORT).show();
        } else {
            ++number;
            play(number);
        }
    }

    //暂停音乐
    private void pause()
    {
        if (player.isPlaying())
        {
            player.pause();
            status = MusicService.STATUS_PAUSED;
            sendBroadcastOnStatusChanged(MusicService.STATUS_PAUSED);
            updateNotification();
        }
    }

    //停止播放
    private void stop()
    {
        if (status != MusicService.STATUS_STOPPED)
        {
            player.stop();
            status = MusicService.STATUS_STOPPED;
            sendBroadcastOnStatusChanged(MusicService.STATUS_STOPPED);
            updateNotification();
        }
    }

    //恢复播放
    private void resume()
    {
        player.start();
        status = MusicService.STATUS_PLAYING;
        sendBroadcastOnStatusChanged(MusicService.STATUS_PLAYING);
        updateNotification();
    }

    //重新播放
    private void replay()
    {
        player.start();
        status = MusicService.STATUS_PLAYING;
        sendBroadcastOnStatusChanged(MusicService.STATUS_PLAYING);
        updateNotification();
    }

    //检查是否正在播放
    private void checkIsPlaying()
    {
        if (player != null && player.isPlaying())
        {
            sendBroadcastOnStatusChanged(MusicService.STATUS_PLAYING);
        }
    }

    //随机播放
    private void random()
    {
        Random random=new Random();
        int i;
        do
        {
            i=random.nextInt(MusicList.getMusicList().size());
        }while(i==number);
        number=i;
        play(number);
    }

    //发送广播，提醒状态改变了
    private void sendBroadcastOnStatusChanged(int status)
    {
        Intent intent = new Intent(BROADCAST_MUSICSERVICE_UPDATE_STATUS);
        intent.putExtra("status", status);
        if(status!=STATUS_STOPPED)
        {
            intent.putExtra("time",player.getCurrentPosition());
            intent.putExtra("duration",player.getDuration());
            intent.putExtra("number",number);
            intent.putExtra("musicName",MusicList.getMusicList().get(number).getmusicName());
            intent.putExtra("musicArtist",MusicList.getMusicList().get(number).getmusicArtist());
        }
        sendBroadcast(intent);
    }

    //电话监听器
    private final class MyPhoneListener extends PhoneStateListener
    {
        public void onCallStateChanged(int state, String incomingNumber) {
            switch (state)
            {
                case TelephonyManager.CALL_STATE_RINGING:
                    if (status == MusicService.STATUS_PLAYING) {
                        pause();
                        phone=true;
                    }
                    break;
                case TelephonyManager.CALL_STATE_IDLE:
                    if (phone){
                        resume();
                        phone=false;
                    }
                    break;
            }
        }
    }

    //播放结束监听器
    OnCompletionListener completionListener = new OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer player)
        {
            if (player.isLooping())
            {
                replay();
            } else {
                sendBroadcastOnStatusChanged(MusicService.STATUS_COMPLETED);
            }
        }
    };

    @Override
    public void onDestroy()
    {
        sendBroadcastOnStatusChanged(MusicService.STATUS_STOPPED);
        manager.cancel(0x3131);
        //释放播放器资源
        if (player != null)
        {
            player.release();
        }
        super.onDestroy();
    }
}