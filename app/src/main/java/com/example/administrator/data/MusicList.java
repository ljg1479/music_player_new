package com.example.administrator.data;

import java.util.ArrayList;

/**
 * Created by winner on 2018/10/10.
 */

//MusicList类，采用单一实例，ֻ只能通过getMusicList方法获取共享，唯一的arrayList<Music>对象
public class MusicList
{
    private static ArrayList<Music> musicarray = new ArrayList<Music>();
    private MusicList(){}

    public static ArrayList<Music> getMusicList()
    {
        return musicarray;
    }
}
