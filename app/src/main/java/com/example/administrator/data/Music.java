package com.example.administrator.data;

/**
 * Created by winner on 2018/10/10.
 */

//Music类，包含歌曲名，艺术家，路径，时长等属性，以及相关的获取方法

public class Music
{
    //歌曲信息
    private String musicName;
    private String musicArtist;
    private String musicPath;
    private String musicDuration;

    //构造函数
    public Music (String musicName,String musicArtist,String musicPath,String musicDuration)
    {
        this.musicName = musicName;
        this.musicArtist = musicArtist;
        this.musicPath = musicPath;
        this.musicDuration = musicDuration;
    }

    //返回函数
    public String getmusicName()
    {
        return this.musicName;
    }
    public String getmusicArtist()
    {
        return this.musicArtist;
    }
    public String getmusicPath()
    {
        return this.musicPath;
    }
    public String getmusicDuration()
    {
        return this.musicDuration;
    }
}