package com.example.administrator.http;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.net.URLEncoder;

/**
 * Created by winner on 2018/10/26.
 */

public class LrcFileDownLoad {
    //歌词API
    public static final String LRC_SEARCH_URL = "http://geci.me/api/lyric/";
    //请求超时
    private static final int REQUEST_TIMEOUT = 15*1000;
    //等待数据超时
    private static final int SO_TIMEOUT = 15*1000;

    //获取歌词url
    public static String getSongLRCUrl(String path, String songName) throws Exception
    {
        String url = null;
        String str_json = null;

        //歌词名为空返回null
        if(songName == null)
        {
            return null;
        }
        //编码转换
        String name = URLEncoder.encode(songName, "UTF-8");
        str_json = getHtmlCode(path + name);

        //超时以及其他异常时返回null
        if(str_json == null )
        {
            return null;
        }

        //json对象
        JSONObject jsonObject = new JSONObject(str_json);
        //获取歌词数目
        int count = jsonObject.getInt("count");

        //没有歌词时返回null
        if (count == 0)
        {
            return null;
        }

        //获取得到是歌词url列表，这里只取第一个歌词的url
        JSONArray jsonArray = jsonObject.getJSONArray("result");
        JSONObject item = jsonArray.getJSONObject(0);

        url = item.getString("lrc");
        return url;
    }

    // 获取网页源码
    public static String getHtmlCode(String path)
    {
        String result = null;
        try
        {
            //设置超时处理
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet get = new HttpGet(path);
            HttpResponse response = httpclient.execute(get);

            //获取成功则逐句加入歌词
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK)
            {
                HttpEntity entity = response.getEntity();
                BufferedReader br = new BufferedReader(new InputStreamReader(entity.getContent(), "utf-8"));
                String line;
                result = "";
                while ((line = br.readLine()) != null)
                {
                    result += line + "\n";
                }
            }
        }
        catch(ConnectTimeoutException e)
        {
            System.out.println("ConnectTimeoutException timeout");
            return null;
        }
        catch (SocketTimeoutException e)
        {
            System.out.println("SocketTimeoutException timeout");
            return null;
        }
        catch (Exception e) {
            System.out.println(e.getMessage() + ":" + e.toString());
            return null;
        }
        return result;
    }
}
